package bcas.ap.exception.demo1;

public class ArithmaticException {
	public static void main(String[] args) {
		int result1 = 5 + 3;
		System.out.println(result1);
		
		
		try {
			int x = 5;
			int y = 0;
			int cal = 5/0;
			System.out.println(cal);
			
		} catch (ArithmeticException ex) {
		 System.out.println(ex.getMessage());
		 System.out.println("Input Value Should Not Be Zero");
		}
		
		int result2 = 6 - 3;
		System.out.println(result2);
		
		
		
		
		
		
		
	}
	
	
	
	

}
