package bcas.ap.exception.demo1;

public class MultipleException {
	public static void main(String[] args) {
		try {
			int x = 5;
			int y = 0;
			int cal = 5 / 0;
			System.out.println(cal);

			String age = null;
			System.out.println(age.length());

		} catch (ArithmeticException ex) {
			System.out.println(ex.getMessage());
			System.out.println("Input Value Should Not Be Zero");
		
		} catch (NullPointerException e) {
			System.out.println("Value Not Be Null");
			System.out.println(e.getMessage());
		}
		finally {
			System.out.println("Last Message");
		}
	}
}
