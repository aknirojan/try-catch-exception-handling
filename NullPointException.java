package bcas.ap.exception.demo1;

public class NullPointException {
	public static void main(String[] args) {
		String name = "Nirojan";
		System.out.println(name.length());

		try {
			String age = null;
			System.out.println(age.length());
		} catch (NullPointerException ex) {
			System.out.println("Value Not Be Null");
			System.out.println(ex.getMessage());
		}
	}
}
